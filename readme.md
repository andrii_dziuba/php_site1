**PHP**  
  
Used technologies:  
- apache server  
- mysql  
- bootstrap  
  
Database dump:  
https://drive.google.com/open?id=1zv0go99tX6CtG0Slaw9KjmHVq5-1hQ7p  
Database name: mydb. Defined in /config/persistant.php  
  
Screens:  
![picture](res/screen1.png)  
![picture](res/screen2.png)  
![picture](res/screen3.png)  