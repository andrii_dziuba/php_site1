﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>



<!-- Modal -->
<div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Feedback</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div id='alert' class='mx-3 mt-2 alert alert-danger d-none'>Please, fill all required fields</div>
		<form id='feedbackForm' action='form.php' method='post' enctype="multipart/form-data">
			<div class="modal-body">
				<div class="form-row">
					<div class="col">
						<input id="inputUserName" type="text" class="form-control" placeholder="Your name (required)" name='username' required>
					</div>
					<div class="col">
						<input id="inputEmail" type="email" class="form-control" placeholder="Email (required)" name='email' required>
					</div>
				</div>
				<div class="form-row my-2">
					<span class='mx-3'>Your point (required):</span>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="radioOption" value='1' required>
						<label class="form-check-label" for="inlineRadio1">1</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="radioOption" value='2'>
						<label class="form-check-label" for="inlineRadio2">2</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="radioOption" value='3'>
						<label class="form-check-label" for="inlineRadio3">3</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="radioOption" value='4'>
						<label class="form-check-label" for="inlineRadio4">4</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="radioOption" value='5'>
						<label class="form-check-label" for="inlineRadio5">5</label>
					</div>
				</div>
				<div class="form-row">
					<div class='col'>
					<textarea id="inputFeedback" class='form-control' rows='8' placeholder='Your feedback (required)' name='feedback' required></textarea>
					</div>
				</div>
				<div class="form-row my-2 mx-2">
					<label class=>Select images (optional):</label>
					<input type="file" name="fileUpload[]" multiple accept='image/*'> 
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button id="submitFeedback" type="submit" class="btn btn-primary float-right">Leave a feedback</button>
			</div>
		</form>
    </div>
  </div>
</div>

<h3 class="text-center m-4">Feedbacks</h3>
<div class="container">
<?php
require_once 'config/persistant.php'; 
require_once 'utils/utils.php';

$nodesPerPage = 1;
$page = 1;
if(isset($_GET['page'])) {
	$page = isNumberNatural((int)$_GET['page']) ? (int)$_GET['page'] : $page;
}

if(isset($_GET['nodesPerPage'])) {
	$nodesPerPage = isNumberNatural((int)$_GET['nodesPerPage']) ? (int)$_GET['nodesPerPage'] : $nodesPerPage;
}	
 
$link = mysqli_connect($host, $user, $password, $database) 
    or die(mysqli_error($link));
	
$countQueryResult = mysqli_query($link, "select count(*) from response");

if($countQueryResult) {
	$feedbacksCount = mysqli_fetch_row($countQueryResult)[0];
	$pages = (int)(ceil($feedbacksCount / $nodesPerPage));
}

//computing average rating 
$query = "select avg(responseRating) from response";
$result = mysqli_query($link, $query);
?>
	<div class='row mb-2'>
		<div class='col'>
			<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#feedbackModal'>
				Add feedback
			</button>
		</div>
		<?php
		$averageRating = 0;
		if($result) {
			$averageRating = round(mysqli_fetch_row($result)[0], 2);
		}
		?>
		<div class='col text-right mt-2'>
			<h5>Average rating: <?php echo $averageRating; ?></h5>
		</div>
	</div>

<?php
if($feedbacksCount !== 0) {
	
	//retrieve all feedbacks (responses)
	$query = "select response.id, responseText, responseRating, 
	userName, userEmail from response join usr on usr.id=response.usr_id order by response.id desc
	limit " . (($page-1)*$nodesPerPage) . ", $nodesPerPage";

	$result = mysqli_query($link, $query) or die(mysqli_error($link)); 
	if($result)
	{    
		while($row = mysqli_fetch_row($result))
		{
			echo "<div class='row border shadow p-2 mb-3 bg-white rounded'";
			echo "<div class='media'>";
			echo "<div class='media-body'>";
			echo "<h5 class='mt-0'><span class='mx-3'><b>$row[3]</b></span>
			<span><i>(<a href='mailto:$row[4]'>$row[4]</a>)</i></span>
			<span class='float-right'>Rating: $row[2]/5</span></h5>";
			echo "<div id='response' class='mx-3'>$row[1]</div>";
			
			$imagesQuery = "select filename from image where response_id = $row[0]";
			$responseImages = mysqli_query($link, $imagesQuery);
			if($responseImages) {
				if(mysqli_num_rows($responseImages) !== 0) {
					echo "<hr class='m-2'/><div class='ml-3 mb-2'>Images:</div><div style='display: inline-block; margin-left: 8px;'>";
					while($img = mysqli_fetch_row($responseImages))
					{
						$image = image_path . $img[0];
						echo "<a href='$image' target='_blank'>";
						echo "<img class='img-collapsed ml-2 zoom' src=$image></img>";
						echo "</a>";
					}
					echo "</div>";
				}
			}
			echo "</div></div>";
		}
		
		 
		mysqli_free_result($result);
	}	
} else {
	echo "No responses yet.";
}
mysqli_close($link);
?>

</div>

<?php //Pagination
// $pages - total amount of pages
if($pages > 1) {
	// 'Previous' button
	echo "<nav aria-label='...'>
	  <ul class='pagination justify-content-center'>
		<li class='page-item "
		. ($page == 1 ? "disabled" : "") .
		"'>
		  <a class='page-link' href='?page=". ($page-1) ."' tabindex='-1'>Previous</a>
		</li>";
		
	if($pages <= 7) {
		
		for($i=1; $i<=$pages; $i++) {
			echoPageButton($i, $page);
		}
	} else {
		$currentPage = $page;
		$totalPages = $pages;
		$head = $currentPage > 4 ? [1, -1] : [1, 2, 3]; 
		$tail = $currentPage < $totalPages - 3 ? [-1, $totalPages] : [$totalPages-2, $totalPages-1, $totalPages];
		$bodyBefore = ($currentPage > 4 and $currentPage < $totalPages-1) ? [$currentPage-2, $currentPage-1] : [];
		$bodyAfter = ($currentPage > 2 and $currentPage < $totalPages-3) ? [$currentPage+1, $currentPage+2] : [];
		
		$pageButtonArray = array_merge($head, $bodyBefore, 
			(($currentPage>3 and $currentPage<$totalPages-2) ? [$currentPage] : []) , 
			$bodyAfter, $tail);
		
		foreach($pageButtonArray as $i) {
			echoPageButton($i, $page);
		}
	}
	
	// 'Next' button
	echo "<li class='page-item "
		. ($page == $pages ? "disabled" : "") .
		"'>
		  <a class='page-link' href='?page=". ($page+1) ."'>Next</a>
		</li>
	  </ul>
	</nav>";
}

function echoPageButton($pageNumber, $currentPage) {
	global $nodesPerPage;
	if($pageNumber == -1) {
		echo "<li class='page-item disabled'>
				<a class='page-link' href='#'>...</a>
			</li>";
	} else 
		echo "<li class='page-item ". ($pageNumber == $currentPage ? "active" : "") ."'>
				<a class='page-link' href='?page=$pageNumber&nodesPerPage=$nodesPerPage'>$pageNumber</a>
			</li>";
}
?>

<div class='row justify-content-center'>
	<ul class='pagination justify-content-center'>
		<li class='page-item disabled'><a class='page-link'>Elements per page:</a></li>
		<?php
			$elementsPerPageArray = [1, 2, 5, 10];
			
			foreach($elementsPerPageArray as $i) {
				echoNodePerPageButton($i);
			}
		?>
	</ul>
<div>

<?php
function echoNodePerPageButton($number) {
	global $nodesPerPage;
	echo "<li class='page-item " . ($nodesPerPage == $number ? "active" : "") . "'>
			<a href='?page=1&nodesPerPage=$number' class='page-link'>$number</a>
		</li>";
}
?>
<script type="text/javascript">
	/*$("#submitFeedback").on('click', function(event) {
		event.preventDefault();
		var radio = $('input[name=radioOption]:checked', '#feedbackForm').val()
		if($("#inputUserName").val() === "" || $("#inputEmail").val() === "" || $("#inputFeedback").val() === "" || radio == undefined) {
			$("#alert").removeClass("d-none");
			$("#alert").addClass("d-inline");
		} else $("#feedbackForm").submit();
	});*/
</script>

</body>

</html>